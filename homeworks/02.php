<?php
namespace MrCeperka\MIPAA\Homeworks;

require __DIR__ . '/../definitions.php';
require __DIR__ . '/../Heuristics.php';
require __DIR__ . '/../FileParser.php';
require __DIR__ . '/../DataProvider.php';
require __DIR__ . '/../ErrorBuilder.php';

use MrCeperka\MIPAA\DataProvider;
use MrCeperka\MIPAA\FileParser;
use MrCeperka\MIPAA\Heuristics\BranchAndBound;
use MrCeperka\MIPAA\Heuristics\BranchAndBoundFractional;
use MrCeperka\MIPAA\Heuristics\DynamicProgrammingByPrice;


//REFACTOR!
$number = 4;
if (!empty($_SERVER['argv'][1]) && file_exists(DATA_DIR . 'knap_' . $_SERVER['argv'][1] . '.inst.dat')) {
	$number = $_SERVER['argv'][1];
}

$filesGenerator = function ($number) {
	foreach (new \DirectoryIterator(DATA_DIR) as $item) {
		if ($item->getExtension() === 'dat'
			&& strpos($item->getFilename(), '_' . $number . '.inst') !== false
		) {
			yield $item->getPathname();
		}
	}
};


$dataGen = function ($filesGenerator, $number) {
	foreach ($filesGenerator($number) as $path) {
		echo $path . PHP_EOL;
		yield (new FileParser($path))->parse();
	}
};

$totals = function (&$merged, $res) {
	if (isset($merged[$res['n']])) {
		$merged[$res['n']]['totalTime'] += $res['time'];
		$merged[$res['n']]['totalCnt']++;
	} else {
		$merged[$res['n']]['totalTime'] = $res['time'];
		$merged[$res['n']]['totalCnt'] = 1;
	}
};

$logTotals = function ($filename, $merged) {
	foreach ($merged as $key => $item) {
		if ($item['totalCnt'] > 0) {
			file_put_contents($filename, $key . ' ' . round($item['totalTime'] / $item['totalCnt']) . "\n", FILE_APPEND);
		}
	}
};


$mergedDNP = [];
$mergedBNB = [];
$mergedBNBF = [];
foreach ((new DataProvider($dataGen($filesGenerator, $number)))->getData() as $generator) {
	
	
	$dnp = new DynamicProgrammingByPrice($generator);
	$dnp->execute();
	$dnp->logResult();
	$dnpResult = $dnp->getResult();
	$totals($mergedDNP, $dnpResult);

	$bnb = new BranchAndBound($generator);
	$bnb->execute();
	$bnb->logResult();
	$bnbResult = $bnb->getResult();
	$totals($mergedBNB, $bnbResult);
	
	$bnbf = new BranchAndBoundFractional($generator);
	$bnbf->execute();
	$bnbf->logResult();
	$bnbfResult = $bnbf->getResult();
	$totals($mergedBNBF, $bnbfResult);
	
}
$logTotals(OUTPUT_DIR . '02_log_dnph.csv', $mergedDNP);
$logTotals(OUTPUT_DIR . '02_log_bnb.csv', $mergedBNB);
$logTotals(OUTPUT_DIR . '02_log_bnbf.csv', $mergedBNBF);