<?php
namespace MrCeperka\MIPAA\Homeworks;

require __DIR__ . '/../definitions.php';
require __DIR__ . '/../Heuristics.php';
require __DIR__ . '/../FileParser.php';
require __DIR__ . '/../DataProvider.php';
require __DIR__ . '/../ErrorBuilder.php';

use MrCeperka\MIPAA\DataProvider;
use MrCeperka\MIPAA\ErrorBuilder;
use MrCeperka\MIPAA\ErrorBuilderException;
use MrCeperka\MIPAA\FileParser;
use MrCeperka\MIPAA\Heuristics\DynamicProgrammingByPrice;


//REFACTOR!
$number = 4;
if (!empty($_SERVER['argv'][1]) && file_exists(DATA_DIR . 'knap_' . $_SERVER['argv'][1] . '.inst.dat')) {
	$number = $_SERVER['argv'][1];
}

$filesGenerator = function ($number) {
	foreach (new \DirectoryIterator(DATA_DIR) as $item) {
		if ($item->getExtension() === 'dat'
			&& strpos($item->getFilename(), '_' . $number . '.inst') !== false
		) {
			yield $item->getPathname();
		}
	}
};


$dataGen = function ($filesGenerator, $number) {
	foreach ($filesGenerator($number) as $path) {
		echo $path . PHP_EOL;
		yield (new FileParser($path))->parse();
	}
};

$totals = function (&$merged, $res) {
	if (isset($merged[$res['n']])) {
		$merged[$res['n']]['totalTime'] += $res['time'];
		$merged[$res['n']]['maxError'] += $res['maxError'];
		$merged[$res['n']]['totalCnt']++;
	} else {
		$merged[$res['n']]['totalTime'] = $res['time'];
		$merged[$res['n']]['totalCnt'] = 1;
		$merged[$res['n']]['maxError'] = $res['maxError'];
	}
};

$logTotals = function ($filename, $merged) {
	foreach ($merged as $key => $item) {
		if ($item['totalCnt'] > 0) {
			file_put_contents($filename, $key . ' ' . round($item['totalTime'] / $item['totalCnt']) . "\n", FILE_APPEND);
		}
	}
};

$logMaxError = function ($filename, $n, $error) {
	file_put_contents($filename, $n . ' ' . $error . PHP_EOL, FILE_APPEND);
};


$mergedDNP = [];
$mergedFPTAS = [];
$errorData = [];

$withFPTAS = isset($_SERVER['argv'][2]) && $_SERVER['argv'][2] == 'fptas' ? true : false;
$error = isset($_SERVER['argv'][3]) && is_numeric($_SERVER['argv'][3]) && $withFPTAS ? $_SERVER['argv'][3] : false;

$fptasMaxError = 0;
foreach ((new DataProvider($dataGen($filesGenerator, $number)))->getData() as $generator) {
		
		$dnp = new DynamicProgrammingByPrice($generator);
		$dnp->execute();
		$dnpResult = $dnp->getResult();
		$totals($mergedDNP, $dnpResult);
		
		$fptas = new DynamicProgrammingByPrice(array_merge($generator, ['fptas' => $withFPTAS, 'error' => $error]));
		$fptas->execute();
		$fptasResult = $fptas->getResult();
		$totals($mergedFPTAS, $fptasResult);
	
		if($fptasResult['maxError'] > $fptasMaxError) {
			$fptasMaxError = $fptasResult['maxError'];
		}
		
		try {
			$errorData[] = ErrorBuilder::build($dnpResult, $fptasResult);
		} catch (ErrorBuilderException $e) {
			//dont care
		}
	
}

//$logTotals(OUTPUT_DIR . '02.1_log_dnp.csv', $mergedDNP);
//$logTotals(OUTPUT_DIR . '02.1_log_fptas.csv', $mergedFPTAS);
$logMaxError(OUTPUT_DIR . '02_max_errors.txt', $number, $fptasMaxError);
try {
	ErrorBuilder::logErrors(OUTPUT_DIR . '02_error.txt', ErrorBuilder::getErrors($errorData));
} catch (ErrorBuilderException $e) {
	echo 'Empty errors' . PHP_EOL;
}
