<?php
namespace MrCeperka\MIPAA\Homeworks;

require __DIR__ . '/../definitions.php';
require __DIR__ . '/../Heuristics.php';
require __DIR__ . '/../FileParser.php';
require __DIR__ . '/../DataProvider.php';
require __DIR__ . '/../ErrorBuilder.php';

use MrCeperka\MIPAA\DataProvider;
use MrCeperka\MIPAA\ErrorBuilder;
use MrCeperka\MIPAA\ErrorBuilderException;
use MrCeperka\MIPAA\FileParser;
use MrCeperka\MIPAA\Heuristics\BranchAndBound;
use MrCeperka\MIPAA\Heuristics\BranchAndBoundFractional;
use MrCeperka\MIPAA\Heuristics\DynamicProgrammingByPrice;
use MrCeperka\MIPAA\Heuristics\PriceVsWeightHeuristics;

if(!isset($_SERVER['argv'][1])) {
	echo 'use it as php 03.php <folder>';
	return;
}
$folder = $_SERVER['argv'][1];


$filesGenerator = function ($folder) {
	foreach (new \DirectoryIterator(__DIR__ . '/../knapgen/' . $folder) as $item) {
		if($item->isFile()) {
			yield $item->getPathname();
		}
	}
};


$dataGen = function ($filesGenerator, $folder) {
	$files = [];
	foreach ($filesGenerator($folder) as $path) {
		$files[] = $path;
	}
	//sort files
	usort($files, function ($a, $b)
	{
		return min(max(strcmp($a, $b), -1), 1);
	});
	
	foreach ($files as $path) {
		echo $path . PHP_EOL;
		yield (new FileParser($path))->parse();
	}
};

$totals = function (&$merged, $res, $n) {
	if (isset($merged[$n])) {
		$merged[$n]['totalTime'] += $res['time'];
		$merged[$n]['totalCnt']++;
	} else {
		$merged[$n]['totalTime'] = $res['time'];
		$merged[$n]['totalCnt'] = 1;
	}
};

$logTotals = function ($filename, $merged) {
	foreach ($merged as $key => $item) {
		if ($item['totalCnt'] > 0) {
			file_put_contents($filename, $key . ' ' . round($item['totalTime'] / $item['totalCnt']) . "\n", FILE_APPEND);
		}
	}
};


$mergedDNP = [];
$mergedBNB = [];
$mergedBNBF = [];
$mergedPVWH = [];
$errorData = [];
$i = 0;
foreach ((new DataProvider($dataGen($filesGenerator, $folder)))->getData() as $key => $generator) {
	if($key % 50 === 0) {
		$i++;
	};
	echo $i . '--' . $generator['ID'] . PHP_EOL;
	
	/*
	$dnp = new DynamicProgrammingByPrice($generator);
	$dnp->execute();
	//$dnp->logResult();
	$dnpResult = $dnp->getResult();
	$totals($mergedDNP, $dnpResult, $folder . $i);
	*/
	
	
	$bnb = new BranchAndBound($generator);
	$bnb->execute();
	//$bnb->logResult();
	$bnbResult = $bnb->getResult();
	$totals($mergedBNB, $bnbResult, $folder . $i);
	
	
	/*
	$bnbf = new BranchAndBoundFractional($generator);
	$bnbf->execute();
	//$bnbf->logResult();
	$bnbfResult = $bnbf->getResult();
	$totals($mergedBNBF, $bnbfResult, $folder . $i);
	
	
	$pvwh = new PriceVsWeightHeuristics($generator);
	$pvwh->execute();
	//$pvwh->logResult();
	$pvwhResult = $pvwh->getResult();
	$totals($mergedPVWH, $pvwhResult, $folder . $i);
	*/
	
	
	
	/*try {
		$errorData[$i][] = ErrorBuilder::build($dnpResult, $pvwhResult);
	} catch (ErrorBuilderException $e) {
	}*/
}

//$logTotals(OUTPUT_DIR . '03' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . '03_log_dnph.csv', $mergedDNP);
$logTotals(OUTPUT_DIR . '03' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . '03_log_bnb.csv', $mergedBNB);
//$logTotals(OUTPUT_DIR . '03' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . '03_log_bnbf.csv', $mergedBNBF);
//$logTotals(OUTPUT_DIR . '03' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . '03_log_pvwh.csv', $mergedPVWH);

foreach ($errorData as $key => $item) {
	ErrorBuilder::logErrors(OUTPUT_DIR . '03' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . '03_error.txt', ErrorBuilder::getErrors($item));
}

/*try {
	ErrorBuilder::logErrors(OUTPUT_DIR . '03' . DIRECTORY_SEPARATOR .  '03_error.txt', ErrorBuilder::getErrors($errorData));
} catch (ErrorBuilderException $e) {
	echo 'Empty errors' . PHP_EOL;
}*/
