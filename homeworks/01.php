<?php
namespace MrCeperka\MIPAA\Homeworks;

require __DIR__ . '/../definitions.php';
require __DIR__ . '/../Heuristics.php';
require __DIR__ . '/../FileParser.php';
require __DIR__ . '/../DataProvider.php';
require __DIR__ . '/../ErrorBuilder.php';

use MrCeperka\MIPAA\DataProvider;
use MrCeperka\MIPAA\ErrorBuilder;
use MrCeperka\MIPAA\ErrorBuilderException;
use MrCeperka\MIPAA\FileParser;
use MrCeperka\MIPAA\Heuristics\NotSoDumbBrutalForce;
use MrCeperka\MIPAA\Heuristics\PriceVsWeightHeuristics;


//REFACTOR!
$number = 4;
if (!empty($_SERVER['argv'][1]) && file_exists(DATA_DIR . 'knap_' . $_SERVER['argv'][1] . '.inst.dat')) {
	$number = $_SERVER['argv'][1];
}

$filesGenerator = function ($number) {
	foreach (new \DirectoryIterator(DATA_DIR) as $item) {
		if ($item->getExtension() === 'dat'
			&& strpos($item->getFilename(), '_' . $number . '.inst') !== false
		) {
			yield $item->getPathname();
		}
	}
};


$dataGen = function ($filesGenerator, $number) {
	foreach ($filesGenerator($number) as $path) {
		echo $path . PHP_EOL;
		yield (new FileParser($path))->parse();
	}
};

$totals = function (&$merged, $res) {
	if (isset($merged[$res['n']])) {
		$merged[$res['n']]['totalTime'] += $res['time'];
		$merged[$res['n']]['totalCnt']++;
	} else {
		$merged[$res['n']]['totalTime'] = $res['time'];
		$merged[$res['n']]['totalCnt'] = 1;
	}
};

$logTotals = function ($filename, $merged) {
	foreach ($merged as $key => $item) {
		if ($item['totalCnt'] > 0) {
			file_put_contents($filename, $key . ' ' . round($item['totalTime'] / $item['totalCnt']) . "\n", FILE_APPEND);
		}
	}
};


$mergedPVWH = [];
$mergedNSDBFH = [];
$errorData = [];
foreach ((new DataProvider($dataGen($filesGenerator, $number)))->getData() as $generator) {
	
	$pvwh = new PriceVsWeightHeuristics($generator);
	$pvwh->execute();
	$pvwh->logResult();
	$pvwhResult = $pvwh->getResult();
	$totals($mergedPVWH, $pvwhResult);
	
	$nsdbfh = new NotSoDumbBrutalForce($generator);
	$nsdbfh->execute();
	$nsdbfh->logResult();
	$nsdbfhResult = $nsdbfh->getResult();
	$totals($mergedNSDBFH, $nsdbfhResult);
	
	
	try {
		$errorData[] = ErrorBuilder::build($nsdbfhResult, $pvwhResult);
	} catch (ErrorBuilderException $e) {
	}
	
}
$logTotals(OUTPUT_DIR . 'nsdbfh.csv', $mergedNSDBFH);
$logTotals(OUTPUT_DIR . 'pvwh.csv', $mergedPVWH);
try {
	ErrorBuilder::logErrors(OUTPUT_DIR . '01_error.txt', ErrorBuilder::getErrors($errorData));
} catch (ErrorBuilderException $e) {
	echo 'Empty errors' . PHP_EOL;
}
