<?php
namespace MrCeperka\MIPAA;

class FileParser
{
	private $filename;
	
	public function __construct($filename)
	{
		$this->filename = $filename;
	}
	
	/**
	 * @return \Generator
	 */
	public function parse()
	{
		$file = fopen($this->filename, 'r');
		while (($line = fgets($file)) !== false) {
			yield str_replace(PHP_EOL, '', $line);
		}
		fclose($file);
	}
}