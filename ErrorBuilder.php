<?php
namespace MrCeperka\MIPAA;
class ErrorBuilderException extends \Exception {}

class ErrorBuilder
{
	public static function build($opt, $apx)
	{
		//for each instance compute relative error
		//ε = ( C(OPT)-C(APX) ) / C(OPT)
		if($opt['sumPrice'] > 0) {
			return ($opt['sumPrice'] - $apx['sumPrice'])/$opt['sumPrice'];
		} else {
			throw new ErrorBuilderException();
		}
	}
	
	public static function getErrors($errors)
	{
		$errorsCount = self::checkCount($errors);
		$max = null;
		$totalError = 0;
		
		for($i = 0; $i < $errorsCount; $i++)  {
			//max error
			if($max === null) {
				$max = $errors[$i];
			}
			if($errors[$i] > $max) {
				$max = $errors[$i];
			}
			
			//total error
			$totalError += abs($errors[$i]);
		}
		
		return ['max' => $max, 'avg' => $totalError/$errorsCount];
	}
	
	public static function logErrors($filename, $errors)
	{
		file_put_contents($filename, 'avg: ' . $errors['avg'] . PHP_EOL . 'max: ' . $errors['max']. PHP_EOL, FILE_APPEND);
	}

	
	/**
	 * @param $errors
	 * @return int
	 * @throws ErrorBuilderException
	 */
	private static function checkCount($errors)
	{
		$errorsCount = count($errors);
		if($errorsCount > 0) {
			return $errorsCount;
		} else {
			throw new ErrorBuilderException();
		}
	}
}