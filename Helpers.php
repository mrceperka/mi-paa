<?php
namespace MrCeperka\MIPAA\Helpers;

/**
 * Class PriceWeightRatioHelper
 * @package MrCeperka\MIPAA\Helpers
 */
class PriceWeightRatioHelper
{
	/**
	 * @param \Closure $pairsGen
	 * @return array
	 */
	public static function run(\Closure $pairsGen)
	{
		$pairs = [];
		
		//compute price vs weight ratio
		foreach ($pairsGen() as $pair) {
			$pairs[] = [
				'weight' => $pair['weight'],
				'price' => $pair['price'],
				'ratio' => $pair['price'] / max(1, $pair['weight'])
			];
		}
		
		//sort by price DESC
		usort($pairs, function ($a, $b) {
			return $a['ratio'] <= $b['ratio'] ? 1 : -1;
		});
		
		return $pairs;
	}
}

/**
 * Class Node
 * @package MrCeperka\MIPAA\Helpers
 * @link http://www.geeksforgeeks.org/branch-and-bound-set-2-implementation-of-01-knapsack/
 */
class Node
{
	/**
	 * @var bool True if node is root node
	 */
	public $isRoot = false;
	
	/**
	 * @var int Level of node in decision tree = index in pairs array
	 */
	public $level = 0;
	
	/**
	 * @var int Sum of prices of nodes on path from root to this node (with node included)
	 */
	public $price = 0;
	
	/**
	 * @var int Sum of weights of nodes on path from root to this one (with node included)
	 */
	public $weight = 0;
	
	/**
	 * @var int Upper bound of maximum price in subtree starting with this node
	 */
	public $bound = 0;
	
	/**
	 * Node constructor.
	 * @param bool $isRoot
	 * @param int $level
	 * @param int $price
	 * @param int $weight
	 * @param int $bound
	 */
	public function __construct($isRoot = false, $level = 0, $price = 0, $weight = 0, $bound = 0)
	{
		$this->isRoot = $isRoot;
		$this->level = $level;
		$this->price = $price;
		$this->weight = $weight;
		$this->bound = $bound;
	}
	
	
}