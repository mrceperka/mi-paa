#!/usr/bin/env bash
./output -I 9000 -n 20 -N 50 -m 0.2 -C 50 -W 50 -k 1 -d 0 2>/dev/null 1>price20/knap_20.inst.dat.P050
./output -I 9000 -n 20 -N 50 -m 0.2 -C 250 -W 50 -k 1 -d 0 2>/dev/null 1>price20/knap_20.inst.dat.P200
./output -I 9000 -n 20 -N 50 -m 0.2 -C 500 -W 50 -k 1 -d 0 2>/dev/null 1>price20/knap_20.inst.dat.P500
./output -I 9000 -n 20 -N 50 -m 0.2 -C 700 -W 50 -k 1 -d 0 2>/dev/null 1>price20/knap_20.inst.dat.P700
./output -I 9000 -n 20 -N 50 -m 0.2 -C 1000 -W 50 -k 1 -d 0 2>/dev/null 1>price20/knap_20.inst.dat.P1000