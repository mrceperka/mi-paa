#!/usr/bin/env bash
./output -I 9000 -n 20 -N 50 -m 0.2 -C 50 -W 50 -k 1 -d 0 2>/dev/null 1>ratio20/knap_20.inst.dat.R2
./output -I 9000 -n 20 -N 50 -m 0.3 -C 50 -W 50 -k 1 -d 0 2>/dev/null 1>ratio20/knap_20.inst.dat.R3
./output -I 9000 -n 20 -N 50 -m 0.5 -C 50 -W 50 -k 1 -d 0 2>/dev/null 1>ratio20/knap_20.inst.dat.R5
./output -I 9000 -n 20 -N 50 -m 0.7 -C 50 -W 50 -k 1 -d 0 2>/dev/null 1>ratio20/knap_20.inst.dat.R7
./output -I 9000 -n 20 -N 50 -m 0.9 -C 50 -W 50 -k 1 -d 0 2>/dev/null 1>ratio20/knap_20.inst.dat.R9