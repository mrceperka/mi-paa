#!/usr/bin/env bash
./output -I 9000 -n 20 -N 50 -m 0.2 -W 50 -C 50 -k 1 -d 0 2>/dev/null 1>weight20/knap_20.inst.dat.W050
./output -I 9000 -n 20 -N 50 -m 0.2 -W 250 -C 50 -k 1 -d 0 2>/dev/null 1>weight20/knap_20.inst.dat.W200
./output -I 9000 -n 20 -N 50 -m 0.2 -W 500 -C 50 -k 1 -d 0 2>/dev/null 1>weight20/knap_20.inst.dat.W500
./output -I 9000 -n 20 -N 50 -m 0.2 -W 700 -C 50 -k 1 -d 0 2>/dev/null 1>weight20/knap_20.inst.dat.W700
./output -I 9000 -n 20 -N 50 -m 0.2 -W 1000 -C 50 -k 1 -d 0 2>/dev/null 1>weight20/knap_20.inst.dat.W1000
