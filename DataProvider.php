<?php
namespace MrCeperka\MIPAA;

class DataProvider
{
	private $generators;
	
	public function __construct($generators)
	{
		$this->generators = $generators;
	}
	
	/**
	 * @return \Generator
	 */
	public function getData()
	{
		foreach($this->generators as $generator) {
			foreach ($generator as $item) {
				$exploded = explode(' ', $item);
				$explodedCnt = count($exploded);
				
				$pairsGen = function () use($exploded, $explodedCnt) {
					for ($i = 3; $i < $explodedCnt; $i = $i + 2) {
						yield [
							'weight' => $exploded[$i],
							'price' => $exploded[$i + 1]
						];
					}
				};
				yield [
					'ID' => $exploded[0],
					'n' => $exploded[1],
					'M' => $exploded[2],
					'pairsGen' => $pairsGen,
					'fptas' => false,
					'error' => false
				];
			}
		}
	}
}